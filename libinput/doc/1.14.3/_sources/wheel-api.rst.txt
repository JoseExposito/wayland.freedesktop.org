.. _wheel_scrolling:

==============================================================================
Wheel scrolling
==============================================================================

libinput provides two events to handle wheel scrolling:

- **LIBINPUT_EVENT_POINTER_AXIS** events are sent for regular wheel clicks,
  usually those representing one detent on the device. These wheel clicks
  usually require a rotation of 15 or 20 degrees.

- **LIBINPUT_EVENT_POINTER_AXIS_WHEEL** events are sent for regular and/or
  high resolution wheel movements. High-resolution events are often 4 or 8
  times more common than wheel clicks and require the device to be switched
  into high-resolution mode. A Linux kernel 5.0 or later is required for these
  to be supported. Where high-resolution wheels are not available, libinput
  sends these events for regular wheel clicks.

.. note:: The **LIBINPUT_EVENT_POINTER_AXIS_WHEEL** event is available since libinput 1.13.

.. note:: **LIBINPUT_EVENT_POINTER_AXIS** events are considered legacy API for wheel scrolling as of libinput 1.13.

Both events have the same APIs to access the data within:

* **libinput_event_pointer_get_axis_value()** returns the angle of movement
  in degrees
* **libinput_event_pointer_get_axis_value_discrete()** returns the number of
  logical wheel clicks (always 0 for high-resolution wheel events).
* **libinput_event_pointer_get_axis_value_v120()** returns a value
  normalized into the 0..±120 range, see below. Any multiple of 120 should
  be treated as one full wheel click.

These two events are separate for historical reasons. Both events are
generated for the same source, but callers should not assume any relation
between the two, i.e. there is no guarantee that an axis event is sent
before or after any specific high-resolution event and vice versa.

------------------------------------------------------------------------------
The v120 Wheel API
------------------------------------------------------------------------------

The "v120" value matches the Windows API for wheel scrolling. Wheel
movements are normalized into multiples (or fractions) of 120 with each
multiple of 120 representing one detent of movement. The "v120" API is the
recommended API for callers that do not care about the exact physical
motion and is the simplest API to handle high-resolution scrolling.

Most wheels provide 24 detents per 360 degree rotation (click angle of 15),
others provide 18 detents per 360 degree rotation (click angle 20). Mice
falling outside these two are rare but do exist. Below is a table showing
the various values for a single event, depending on the click angle of the
wheel:

+-------------+------------+---------------+------+
| Click angle | Axis value | Discrete value| v120 |
+=============+============+===============+======+
| 15          |      15    | 1             | 120  |
+-------------+------------+---------------+------+
| 20          |      20    | 1             | 120  |
+-------------+------------+---------------+------+

Fast scrolling may trigger cover than one detent per event and thus each
event may contain multiples of the value, discrete or v120 value:

+-------------+------------+---------------+------+
| Click angle | Axis value | Discrete value| v120 |
+=============+============+===============+======+
| 15          |      30    | 2             |  240 |
+-------------+------------+---------------+------+
| 20          |      60    | 3             |  360 |
+-------------+------------+---------------+------+

Scrolling on high-resolution wheels will produce fractions of 120, depending
on the resolution of the wheel. The example below shows a mouse with click
angle 15 and a resolution of 3 events per wheel click and a mouse with click
angle 20 and a resolution of 2 events per wheel click.

+-------------+------------+---------------+------+
| Click angle | Axis value | Discrete value| v120 |
+=============+============+===============+======+
| 15          |      5     | 0             | 40   |
+-------------+------------+---------------+------+
| 20          |     10     | 0             | 60   |
+-------------+------------+---------------+------+




------------------------------------------------------------------------------
Event sequences for high-resolution wheel mice
------------------------------------------------------------------------------

High-resolution scroll wheels provide multiple events for each detent is
hit. For those mice, an event sequence covering two detents may look like
this:

+--------------+---------+------------+---------------+------+
| Event number |   Type  | Axis value | Discrete value| v120 |
+==============+=========+============+===============+======+
| 1            |  WHEEL  |      5     | 0             | 40   |
+--------------+---------+------------+---------------+------+
| 2            |  WHEEL  |      5     | 0             | 40   |
+--------------+---------+------------+---------------+------+
| 3            |  WHEEL  |      5     | 0             | 40   |
+--------------+---------+------------+---------------+------+
| 4            |  AXIS   |     15     | 1             | 120  |
+--------------+---------+------------+---------------+------+
| 5            |  WHEEL  |      5     | 0             | 40   |
+--------------+---------+------------+---------------+------+
| 6            |  WHEEL  |      5     | 0             | 40   |
+--------------+---------+------------+---------------+------+
| 7            |  AXIS   |     15     | 1             | 120  |
+--------------+---------+------------+---------------+------+

The above assumes a click angle of 15 for the physical detents. Note how the
second set of high-resolution events do **not** add up to a multiple of
120 before the low-resolution event. A caller must not assume that
low-resolution events appear at every multiple of 120.

Fast-scrolling on a high-resolution mouse may trigger multiple fractions per
hardware scanout cycle and result in an event sequence like this:

+---------------+---------+------------+---------------+------+
| Event number  |   Type  | Axis value | Discrete value| v120 |
+===============+=========+============+===============+======+
| 1             |  WHEEL  |      5     | 0             | 40   |
+---------------+---------+------------+---------------+------+
| 2             |  WHEEL  |     10     | 0             | 80   |
+---------------+---------+------------+---------------+------+
| 3             |  AXIS   |     15     | 1             | 120  |
+---------------+---------+------------+---------------+------+
| 4             |  WHEEL  |     10     | 0             | 80   |
+---------------+---------+------------+---------------+------+
| 5             |  WHEEL  |     10     | 0             | 80   |
+---------------+---------+------------+---------------+------+
| 6             |  AXIS   |     15     | 1             | 120  |
+---------------+---------+------------+---------------+------+
| 7             |  AXIS   |      5     | 0             | 40   |
+---------------+---------+------------+---------------+------+

Note how the first low-resolution event is sent at an accumulated 15
degrees, the second at an accumulated 20 degrees. The libinput API does not
specify the smallest fraction a wheel supports.

------------------------------------------------------------------------------
Event sequences for high-resolution wheel mice
------------------------------------------------------------------------------

**LIBINPUT_EVENT_POINTER_AXIS_WHEEL** for low-resolution mice are virtually
identical to **LIBINPUT_EVENT_POINTER_AXIS** events. Note that the discrete
value is always 0 for **LIBINPUT_EVENT_POINTER_AXIS_WHEEL**.

+--------------+---------+------------+---------------+------+
| Event number |   Type  | Axis value | Discrete value| v120 |
+==============+=========+============+===============+======+
| 1            |  AXIS   |     15     | 1             | 120  |
+--------------+---------+------------+---------------+------+
| 2            |  WHEEL  |     15     | 0             | 120  |
+--------------+---------+------------+---------------+------+
| 3            |  WHEEL  |     15     | 0             | 120  |
+--------------+---------+------------+---------------+------+
| 4            |  AXIS   |     15     | 1             | 120  |
+--------------+---------+------------+---------------+------+

Note that the order of **LIBINPUT_EVENT_POINTER_AXIS** vs
**LIBINPUT_EVENT_POINTER_AXIS_WHEEL** events is not guaranteed, as shown in
the example above.

------------------------------------------------------------------------------
Legacy wheel axis events
------------------------------------------------------------------------------

.. warning:: This section only applies for **LIBINPUT_EVENT_POINTER_AXIS**
	events with the axis source **LIBINPUT_POINTER_AXIS_SOURCE_WHEEL**.
	The **LIBINPUT_EVENT_POINTER_AXIS** event is also used for finger- or
	button-based scrolling. Check **libinput_event_pointer_get_axis_source()**
	to determine if an event is a wheel event.

The behavior of **LIBINPUT_EVENT_POINTER_AXIS** events does not change with
the introduction of high-resolution scrolling. These events are generated
for every logical click of the mouse wheel (but not for fractions of a click
on high-resolution scroll wheel mice).

libinput does not provide a mechanism to match legacy events with the new
**LIBINPUT_EVENT_POINTER_AXIS_WHEEL** events. Callers should treat the
sources as independent. Where the caller needs to emulate low-resolution
wheel clicks, it may do so by handling **LIBINPUT_EVENT_POINTER_AXIS_WHEEL**.

Where the caller does not require low-resolution wheel click emulation, it
should ignore all **LIBINPUT_EVENT_POINTER_AXIS_WHEEL** events
with a source of **LIBINPUT_POINTER_AXIS_SOURCE_WHEEL**.
